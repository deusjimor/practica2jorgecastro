package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JSlider;
import javax.swing.JMenuItem;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import java.awt.Checkbox;
import java.awt.ScrollPane;
import java.awt.Toolkit;
import javax.swing.ButtonGroup;

public class VentanaEclipse extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setTitle("TOTAL LEGIT SOFTWARE");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Jacin\\Pictures\\edicion\\f2m_cfinal.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 515, 344);
		contentPane = new JPanel();
		contentPane.setBackground(Color.MAGENTA);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(20, 142, 302, 25);
		contentPane.add(menuBar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(Color.PINK);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"MegaUpload", "Mega", "MediaFire", "Torrent", "Emule", "Ares"}));
		menuBar.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Windows 10", "Windows 8", "Windows Vista", "Windows 7", "Windows XP", "Windows 98"}));
		comboBox_1.setBackground(Color.PINK);
		menuBar.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Office", "Winrar", "Avast", "Spotify"}));
		comboBox_2.setBackground(Color.PINK);
		menuBar.add(comboBox_2);
		
		JSpinner spinner = new JSpinner();
		menuBar.add(spinner);
		
		JSlider slider = new JSlider();
		slider.setBounds(0, 279, 499, 26);
		contentPane.add(slider);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 47, 162, 84);
		contentPane.add(scrollPane);
		
		JEditorPane dtrpnHola = new JEditorPane();
		dtrpnHola.setBackground(Color.CYAN);
		dtrpnHola.setText("Su descarga esta a punto de comenzar\r\nSiga los pasos\r\n1. Seleccione su descargador\r\n2. Elija la su dispositivo\r\n3. Acontinuacion elija el programa a descargar\r\n4. Introduzca su version\r\n5. Establezca la ruta de descarga\r\n6. Acepte el contrato\r\nSi no funciona la version de descarga\r\ncambie la version del programa");
		scrollPane.setViewportView(dtrpnHola);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 462, 23);
		contentPane.add(toolBar);
		
		JButton btnBoton = new JButton("Archivos");
		btnBoton.setBackground(Color.WHITE);
		toolBar.add(btnBoton);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separator);
		
		JButton btnBoton_1 = new JButton("Menu");
		btnBoton_1.setBackground(Color.WHITE);
		toolBar.add(btnBoton_1);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separator_1);
		
		JButton btnBoton_2 = new JButton("Preferencias");
		btnBoton_2.setBackground(Color.WHITE);
		toolBar.add(btnBoton_2);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separator_2);
		
		JButton btnOpciones = new JButton("Opciones");
		btnOpciones.setBackground(Color.RED);
		toolBar.add(btnOpciones);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separator_3);
		
		JButton btnAyuda = new JButton("Ayuda");
		btnAyuda.setBackground(Color.RED);
		toolBar.add(btnAyuda);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separator_4);
		
		JButton btnSecciones = new JButton("Secciones");
		btnSecciones.setBackground(Color.CYAN);
		toolBar.add(btnSecciones);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separator_5);
		
		JButton btnComplementos = new JButton("Complementos");
		btnComplementos.setBackground(Color.CYAN);
		toolBar.add(btnComplementos);
		
		Checkbox checkbox = new Checkbox("Acepto el contrato");
		checkbox.setBounds(380, 247, 109, 22);
		contentPane.add(checkbox);
		
		JRadioButtonMenuItem rdbtnmntmNewRadioItem = new JRadioButtonMenuItem("New radio item");
		rdbtnmntmNewRadioItem.setEnabled(false);
		rdbtnmntmNewRadioItem.setBounds(337, 24, 152, 153);
		contentPane.add(rdbtnmntmNewRadioItem);
		rdbtnmntmNewRadioItem.setIcon(new ImageIcon("C:\\Users\\Jacin\\Pictures\\edicion\\EA_logo.png"));
		
		JMenu mnSeleciona = new JMenu("Idioma");
		mnSeleciona.setBounds(35, 174, 107, 22);
		contentPane.add(mnSeleciona);
		
		JRadioButton rdbtnHombre_1 = new JRadioButton("Espa\u00F1ol");
		mnSeleciona.add(rdbtnHombre_1);
		
		JRadioButton rdbtnMujer_1 = new JRadioButton("Ingles");
		mnSeleciona.add(rdbtnMujer_1);
		
		JRadioButton rdbtnRuso = new JRadioButton("Ruso");
		mnSeleciona.add(rdbtnRuso);
		
		JRadioButton rdbtnOpcionA = new JRadioButton("Opcion A");
		buttonGroup.add(rdbtnOpcionA);
		rdbtnOpcionA.setBackground(Color.GREEN);
		rdbtnOpcionA.setBounds(20, 221, 98, 23);
		contentPane.add(rdbtnOpcionA);
		
		JRadioButton rdbtnOpcionB = new JRadioButton("Opcion B");
		buttonGroup.add(rdbtnOpcionB);
		rdbtnOpcionB.setBackground(Color.GREEN);
		rdbtnOpcionB.setBounds(20, 247, 98, 23);
		contentPane.add(rdbtnOpcionB);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(269, 227, 220, 14);
		contentPane.add(progressBar);
		
		JEditorPane dtrpnRutaDeDescarga = new JEditorPane();
		dtrpnRutaDeDescarga.setBounds(269, 196, 220, 20);
		contentPane.add(dtrpnRutaDeDescarga);
		
		JTextPane txtpnRutaDeDescarga = new JTextPane();
		txtpnRutaDeDescarga.setBackground(Color.MAGENTA);
		txtpnRutaDeDescarga.setText("Ruta de Descarga:");
		txtpnRutaDeDescarga.setBounds(269, 176, 109, 20);
		contentPane.add(txtpnRutaDeDescarga);
	}
}
