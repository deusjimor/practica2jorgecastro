﻿namespace VentanaVisual
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.opcion1ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ejemploDeOpcion1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ejemploDeOpcion2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ejemploDeOpcion3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcion2ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ejemploDeOpcion1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ejemploDeOpcion2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ejemploDeOpcion3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compilarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depurarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seleccionarArchivoDelEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boodlatetarPendriveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boodloateoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formateoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seleccionarProgramaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programaExistenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarEnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarTodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elementoDelEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elementoDeOtraUnidadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlArchivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlArchivoRecienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlTipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlMiembroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlSimboloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busquedaRapidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reemplazoRapidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarEnArchivosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reemplzarEnArchivosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codigoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cuadroDeHerramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compilarSolucionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limpiarSolucionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recopilarSolucionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iniciarDepuracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iniciarSinDepurarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrarConexionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verAyudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarComentariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarProgramaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.soporteTecnicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcion1ToolStripMenuItem2,
            this.opcion2ToolStripMenuItem2,
            this.verToolStripMenuItem,
            this.compilarToolStripMenuItem,
            this.depurarToolStripMenuItem,
            this.equipoToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(558, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // opcion1ToolStripMenuItem2
            // 
            this.opcion1ToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejemploDeOpcion1ToolStripMenuItem,
            this.ejemploDeOpcion2ToolStripMenuItem,
            this.ejemploDeOpcion3ToolStripMenuItem,
            this.guardarToolStripMenuItem,
            this.guardarEnToolStripMenuItem,
            this.guardarTodoToolStripMenuItem});
            this.opcion1ToolStripMenuItem2.Name = "opcion1ToolStripMenuItem2";
            this.opcion1ToolStripMenuItem2.Size = new System.Drawing.Size(65, 20);
            this.opcion1ToolStripMenuItem2.Text = "Archivos";
            this.opcion1ToolStripMenuItem2.Click += new System.EventHandler(this.opcion1ToolStripMenuItem2_Click);
            // 
            // ejemploDeOpcion1ToolStripMenuItem
            // 
            this.ejemploDeOpcion1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.boodlatetarPendriveToolStripMenuItem,
            this.seleccionarArchivoDelEquipoToolStripMenuItem,
            this.boodloateoToolStripMenuItem,
            this.formateoToolStripMenuItem});
            this.ejemploDeOpcion1ToolStripMenuItem.Name = "ejemploDeOpcion1ToolStripMenuItem";
            this.ejemploDeOpcion1ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ejemploDeOpcion1ToolStripMenuItem.Text = "Nuevo";
            // 
            // ejemploDeOpcion2ToolStripMenuItem
            // 
            this.ejemploDeOpcion2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seleccionarProgramaToolStripMenuItem,
            this.programaExistenteToolStripMenuItem});
            this.ejemploDeOpcion2ToolStripMenuItem.Name = "ejemploDeOpcion2ToolStripMenuItem";
            this.ejemploDeOpcion2ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ejemploDeOpcion2ToolStripMenuItem.Text = "Abrir";
            // 
            // ejemploDeOpcion3ToolStripMenuItem
            // 
            this.ejemploDeOpcion3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.elementoDelEquipoToolStripMenuItem,
            this.elementoDeOtraUnidadToolStripMenuItem});
            this.ejemploDeOpcion3ToolStripMenuItem.Name = "ejemploDeOpcion3ToolStripMenuItem";
            this.ejemploDeOpcion3ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ejemploDeOpcion3ToolStripMenuItem.Text = "Agregar";
            // 
            // opcion2ToolStripMenuItem2
            // 
            this.opcion2ToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejemploDeOpcion1ToolStripMenuItem1,
            this.ejemploDeOpcion2ToolStripMenuItem1,
            this.ejemploDeOpcion3ToolStripMenuItem1,
            this.copiarToolStripMenuItem,
            this.pegarToolStripMenuItem});
            this.opcion2ToolStripMenuItem2.Name = "opcion2ToolStripMenuItem2";
            this.opcion2ToolStripMenuItem2.Size = new System.Drawing.Size(50, 20);
            this.opcion2ToolStripMenuItem2.Text = "Editor";
            // 
            // ejemploDeOpcion1ToolStripMenuItem1
            // 
            this.ejemploDeOpcion1ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.irAToolStripMenuItem,
            this.irAlArchivoToolStripMenuItem,
            this.irAlArchivoRecienteToolStripMenuItem,
            this.irAlTipoToolStripMenuItem,
            this.irAlMiembroToolStripMenuItem,
            this.irAlSimboloToolStripMenuItem});
            this.ejemploDeOpcion1ToolStripMenuItem1.Name = "ejemploDeOpcion1ToolStripMenuItem1";
            this.ejemploDeOpcion1ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.ejemploDeOpcion1ToolStripMenuItem1.Text = "Ir a";
            this.ejemploDeOpcion1ToolStripMenuItem1.Click += new System.EventHandler(this.ejemploDeOpcion1ToolStripMenuItem1_Click);
            // 
            // ejemploDeOpcion2ToolStripMenuItem1
            // 
            this.ejemploDeOpcion2ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.busquedaRapidaToolStripMenuItem,
            this.reemplazoRapidoToolStripMenuItem,
            this.buscarEnArchivosToolStripMenuItem,
            this.reemplzarEnArchivosToolStripMenuItem});
            this.ejemploDeOpcion2ToolStripMenuItem1.Name = "ejemploDeOpcion2ToolStripMenuItem1";
            this.ejemploDeOpcion2ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.ejemploDeOpcion2ToolStripMenuItem1.Text = "Buscar y reemplazar";
            // 
            // ejemploDeOpcion3ToolStripMenuItem1
            // 
            this.ejemploDeOpcion3ToolStripMenuItem1.Name = "ejemploDeOpcion3ToolStripMenuItem1";
            this.ejemploDeOpcion3ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.ejemploDeOpcion3ToolStripMenuItem1.Text = "Cortar";
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codigoToolStripMenuItem,
            this.cuadroDeHerramientasToolStripMenuItem});
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.verToolStripMenuItem.Text = "Ver";
            // 
            // compilarToolStripMenuItem
            // 
            this.compilarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compilarSolucionToolStripMenuItem,
            this.limpiarSolucionToolStripMenuItem,
            this.recopilarSolucionToolStripMenuItem});
            this.compilarToolStripMenuItem.Name = "compilarToolStripMenuItem";
            this.compilarToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.compilarToolStripMenuItem.Text = "Compilar";
            // 
            // depurarToolStripMenuItem
            // 
            this.depurarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarDepuracionToolStripMenuItem,
            this.iniciarSinDepurarToolStripMenuItem});
            this.depurarToolStripMenuItem.Name = "depurarToolStripMenuItem";
            this.depurarToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.depurarToolStripMenuItem.Text = "Depurar";
            // 
            // equipoToolStripMenuItem
            // 
            this.equipoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrarConexionesToolStripMenuItem});
            this.equipoToolStripMenuItem.Name = "equipoToolStripMenuItem";
            this.equipoToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.equipoToolStripMenuItem.Text = "Equipo";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verAyudaToolStripMenuItem,
            this.enviarComentariosToolStripMenuItem,
            this.registrarProgramaToolStripMenuItem,
            this.soporteTecnicoToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // seleccionarArchivoDelEquipoToolStripMenuItem
            // 
            this.seleccionarArchivoDelEquipoToolStripMenuItem.Name = "seleccionarArchivoDelEquipoToolStripMenuItem";
            this.seleccionarArchivoDelEquipoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.seleccionarArchivoDelEquipoToolStripMenuItem.Text = "Nuevo programa";
            this.seleccionarArchivoDelEquipoToolStripMenuItem.Click += new System.EventHandler(this.seleccionarArchivoDelEquipoToolStripMenuItem_Click);
            // 
            // boodlatetarPendriveToolStripMenuItem
            // 
            this.boodlatetarPendriveToolStripMenuItem.Name = "boodlatetarPendriveToolStripMenuItem";
            this.boodlatetarPendriveToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.boodlatetarPendriveToolStripMenuItem.Text = "Boodlatetar Pendrive";
            // 
            // boodloateoToolStripMenuItem
            // 
            this.boodloateoToolStripMenuItem.Name = "boodloateoToolStripMenuItem";
            this.boodloateoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.boodloateoToolStripMenuItem.Text = "Boodloateo";
            // 
            // formateoToolStripMenuItem
            // 
            this.formateoToolStripMenuItem.Name = "formateoToolStripMenuItem";
            this.formateoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.formateoToolStripMenuItem.Text = "Formateo";
            // 
            // seleccionarProgramaToolStripMenuItem
            // 
            this.seleccionarProgramaToolStripMenuItem.Name = "seleccionarProgramaToolStripMenuItem";
            this.seleccionarProgramaToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.seleccionarProgramaToolStripMenuItem.Text = "Seleccionar programa";
            // 
            // programaExistenteToolStripMenuItem
            // 
            this.programaExistenteToolStripMenuItem.Name = "programaExistenteToolStripMenuItem";
            this.programaExistenteToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.programaExistenteToolStripMenuItem.Text = "Programa existente";
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.guardarToolStripMenuItem.Text = "Guardar";
            // 
            // guardarEnToolStripMenuItem
            // 
            this.guardarEnToolStripMenuItem.Name = "guardarEnToolStripMenuItem";
            this.guardarEnToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.guardarEnToolStripMenuItem.Text = "Guardar en ...";
            // 
            // guardarTodoToolStripMenuItem
            // 
            this.guardarTodoToolStripMenuItem.Name = "guardarTodoToolStripMenuItem";
            this.guardarTodoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.guardarTodoToolStripMenuItem.Text = "Guardar todo";
            // 
            // elementoDelEquipoToolStripMenuItem
            // 
            this.elementoDelEquipoToolStripMenuItem.Name = "elementoDelEquipoToolStripMenuItem";
            this.elementoDelEquipoToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.elementoDelEquipoToolStripMenuItem.Text = "Elemento del equipo";
            // 
            // elementoDeOtraUnidadToolStripMenuItem
            // 
            this.elementoDeOtraUnidadToolStripMenuItem.Name = "elementoDeOtraUnidadToolStripMenuItem";
            this.elementoDeOtraUnidadToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.elementoDeOtraUnidadToolStripMenuItem.Text = "Elemento de otra unidad";
            // 
            // copiarToolStripMenuItem
            // 
            this.copiarToolStripMenuItem.Name = "copiarToolStripMenuItem";
            this.copiarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.copiarToolStripMenuItem.Text = "Copiar";
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pegarToolStripMenuItem.Text = "Pegar";
            // 
            // irAToolStripMenuItem
            // 
            this.irAToolStripMenuItem.Name = "irAToolStripMenuItem";
            this.irAToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.irAToolStripMenuItem.Text = "Ir a ...";
            // 
            // irAlArchivoToolStripMenuItem
            // 
            this.irAlArchivoToolStripMenuItem.Name = "irAlArchivoToolStripMenuItem";
            this.irAlArchivoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.irAlArchivoToolStripMenuItem.Text = "Ir al archivo ...";
            // 
            // irAlArchivoRecienteToolStripMenuItem
            // 
            this.irAlArchivoRecienteToolStripMenuItem.Name = "irAlArchivoRecienteToolStripMenuItem";
            this.irAlArchivoRecienteToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.irAlArchivoRecienteToolStripMenuItem.Text = "Ir al archivo reciente ...";
            // 
            // irAlTipoToolStripMenuItem
            // 
            this.irAlTipoToolStripMenuItem.Name = "irAlTipoToolStripMenuItem";
            this.irAlTipoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.irAlTipoToolStripMenuItem.Text = "Ir al tipo ...";
            // 
            // irAlMiembroToolStripMenuItem
            // 
            this.irAlMiembroToolStripMenuItem.Name = "irAlMiembroToolStripMenuItem";
            this.irAlMiembroToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.irAlMiembroToolStripMenuItem.Text = "Ir al miembro ...";
            // 
            // irAlSimboloToolStripMenuItem
            // 
            this.irAlSimboloToolStripMenuItem.Name = "irAlSimboloToolStripMenuItem";
            this.irAlSimboloToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.irAlSimboloToolStripMenuItem.Text = "Ir al simbolo ...";
            // 
            // busquedaRapidaToolStripMenuItem
            // 
            this.busquedaRapidaToolStripMenuItem.Name = "busquedaRapidaToolStripMenuItem";
            this.busquedaRapidaToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.busquedaRapidaToolStripMenuItem.Text = "Busqueda rapida";
            // 
            // reemplazoRapidoToolStripMenuItem
            // 
            this.reemplazoRapidoToolStripMenuItem.Name = "reemplazoRapidoToolStripMenuItem";
            this.reemplazoRapidoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.reemplazoRapidoToolStripMenuItem.Text = "Reemplazo rapido";
            // 
            // buscarEnArchivosToolStripMenuItem
            // 
            this.buscarEnArchivosToolStripMenuItem.Name = "buscarEnArchivosToolStripMenuItem";
            this.buscarEnArchivosToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.buscarEnArchivosToolStripMenuItem.Text = "Buscar en archivos";
            // 
            // reemplzarEnArchivosToolStripMenuItem
            // 
            this.reemplzarEnArchivosToolStripMenuItem.Name = "reemplzarEnArchivosToolStripMenuItem";
            this.reemplzarEnArchivosToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.reemplzarEnArchivosToolStripMenuItem.Text = "Reemplzar en archivos";
            // 
            // codigoToolStripMenuItem
            // 
            this.codigoToolStripMenuItem.Name = "codigoToolStripMenuItem";
            this.codigoToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.codigoToolStripMenuItem.Text = "Codigo";
            // 
            // cuadroDeHerramientasToolStripMenuItem
            // 
            this.cuadroDeHerramientasToolStripMenuItem.Name = "cuadroDeHerramientasToolStripMenuItem";
            this.cuadroDeHerramientasToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.cuadroDeHerramientasToolStripMenuItem.Text = "Cuadro de herramientas";
            // 
            // compilarSolucionToolStripMenuItem
            // 
            this.compilarSolucionToolStripMenuItem.Name = "compilarSolucionToolStripMenuItem";
            this.compilarSolucionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.compilarSolucionToolStripMenuItem.Text = "Compilar solucion";
            // 
            // limpiarSolucionToolStripMenuItem
            // 
            this.limpiarSolucionToolStripMenuItem.Name = "limpiarSolucionToolStripMenuItem";
            this.limpiarSolucionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.limpiarSolucionToolStripMenuItem.Text = "Limpiar solucion";
            // 
            // recopilarSolucionToolStripMenuItem
            // 
            this.recopilarSolucionToolStripMenuItem.Name = "recopilarSolucionToolStripMenuItem";
            this.recopilarSolucionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.recopilarSolucionToolStripMenuItem.Text = "Recopilar solucion";
            // 
            // iniciarDepuracionToolStripMenuItem
            // 
            this.iniciarDepuracionToolStripMenuItem.Name = "iniciarDepuracionToolStripMenuItem";
            this.iniciarDepuracionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.iniciarDepuracionToolStripMenuItem.Text = "Iniciar depuracion";
            // 
            // iniciarSinDepurarToolStripMenuItem
            // 
            this.iniciarSinDepurarToolStripMenuItem.Name = "iniciarSinDepurarToolStripMenuItem";
            this.iniciarSinDepurarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.iniciarSinDepurarToolStripMenuItem.Text = "Iniciar sin depurar";
            // 
            // administrarConexionesToolStripMenuItem
            // 
            this.administrarConexionesToolStripMenuItem.Name = "administrarConexionesToolStripMenuItem";
            this.administrarConexionesToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.administrarConexionesToolStripMenuItem.Text = "Administrar conexiones";
            // 
            // verAyudaToolStripMenuItem
            // 
            this.verAyudaToolStripMenuItem.Name = "verAyudaToolStripMenuItem";
            this.verAyudaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.verAyudaToolStripMenuItem.Text = "Ver ayuda";
            // 
            // enviarComentariosToolStripMenuItem
            // 
            this.enviarComentariosToolStripMenuItem.Name = "enviarComentariosToolStripMenuItem";
            this.enviarComentariosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.enviarComentariosToolStripMenuItem.Text = "Enviar comentarios";
            // 
            // registrarProgramaToolStripMenuItem
            // 
            this.registrarProgramaToolStripMenuItem.Name = "registrarProgramaToolStripMenuItem";
            this.registrarProgramaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.registrarProgramaToolStripMenuItem.Text = "Registrar programa";
            // 
            // soporteTecnicoToolStripMenuItem
            // 
            this.soporteTecnicoToolStripMenuItem.Name = "soporteTecnicoToolStripMenuItem";
            this.soporteTecnicoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.soporteTecnicoToolStripMenuItem.Text = "Soporte tecnico";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Cyan;
            this.textBox1.Location = new System.Drawing.Point(319, 44);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(227, 76);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(471, 131);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Español";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(471, 160);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Ingles";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(471, 189);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Ruso";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(343, 131);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(110, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "MegaUpload",
            "Mega",
            "MediaFire",
            "Torrent",
            "Emule",
            "Ares"});
            this.comboBox1.Location = new System.Drawing.Point(106, 44);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(104, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Windows 10",
            "Windows 8",
            "Windows Vista",
            "Windows 7",
            "Windows XP",
            "Windows 98",
            "Linux",
            "Mac"});
            this.comboBox2.Location = new System.Drawing.Point(89, 71);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 8;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Descargador:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Version SO";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Programa";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Office",
            "Winrar",
            "Avast",
            "Spotify"});
            this.comboBox3.Location = new System.Drawing.Point(88, 99);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(122, 21);
            this.comboBox3.TabIndex = 12;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(26, 265);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(69, 17);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Opcion A";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(26, 288);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(69, 17);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Opcion B";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(26, 311);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(68, 17);
            this.radioButton3.TabIndex = 15;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Opcion 3";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Ruta";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(59, 138);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(219, 20);
            this.textBox2.TabIndex = 17;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(157, 306);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(388, 22);
            this.progressBar1.TabIndex = 18;
            this.progressBar1.Value = 20;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(26, 193);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(130, 17);
            this.checkBox1.TabIndex = 20;
            this.checkBox1.Text = "Acpto las condiciones";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOrchid;
            this.ClientSize = new System.Drawing.Size(558, 350);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem opcion1ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ejemploDeOpcion1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcion2ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ejemploDeOpcion2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ejemploDeOpcion3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ejemploDeOpcion1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ejemploDeOpcion2ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ejemploDeOpcion3ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem seleccionarArchivoDelEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compilarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depurarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boodlatetarPendriveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boodloateoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formateoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seleccionarProgramaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programaExistenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elementoDelEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elementoDeOtraUnidadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarEnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarTodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlArchivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlArchivoRecienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlTipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlMiembroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlSimboloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busquedaRapidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reemplazoRapidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarEnArchivosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reemplzarEnArchivosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem codigoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cuadroDeHerramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compilarSolucionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limpiarSolucionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recopilarSolucionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iniciarDepuracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iniciarSinDepurarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrarConexionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verAyudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarComentariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarProgramaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem soporteTecnicoToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

