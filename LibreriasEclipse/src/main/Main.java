package main;
import java.util.Scanner;
import libreriaseclipse.LibreriasEclipse;
public class Main {
	 
	public static void main(String[] args) {
		Scanner entrada=new Scanner(System.in);
		
		System.out.println("Menu"
				+ "\n1. Introducir nombres"
				+ "\n2. Salida nombres"
				+ "\n3. Dar vuelta a un nombre"
				+ "\n4. Modificar un nombre");
		
		String[] nombres=new String[5];
		int menu;
		String contraseñaA="no";
		String contraseñaB;
		do{
			menu=entrada.nextInt();
			switch (menu) {
			case 1: LibreriasEclipse.introducirDatos(nombres);
				break;
			case 2: LibreriasEclipse.salidaDatos(nombres);
				break;
			case 3: LibreriasEclipse.darvueltaNombre(nombres);
				break;
			case 4: LibreriasEclipse.modificarValor(nombres);
				break;

			default:
				System.out.println("Opcion no aceptada");
				break;
			}
			System.out.println("¿Deseas seguir?");
			entrada.nextLine();
			contraseñaB=entrada.nextLine();
			
		}while((contraseñaA).equalsIgnoreCase(contraseñaB)==false);
		

	}
	

	
	
	
	
}